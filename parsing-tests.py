from XML_translations_header_generator import *
import unittest

class TestCreatePrettyLabel(unittest.TestCase):
	def test_1(self):
		self.assertEqual(createPrettyLabel("MYSpace"), "MY Space")

	def test_2(self):
		self.assertEqual(createPrettyLabel("MYSPACE"), "MYSPACE")

	def test_3(self):
		self.assertEqual(createPrettyLabel("My Space"), "My Space")

	def test_4(self):
		self.assertEqual(createPrettyLabel("MySPACE"), "My SPACE")

	def test_5(self):
		self.assertEqual(createPrettyLabel("MySPace"), "My S Pace")

	def test_6(self):
		self.assertEqual(createPrettyLabel("MySPACE"), "My SPACE")

	def test_7(self):
		self.assertEqual(createPrettyLabel("MySpACE"), "My Sp ACE")

	def test_8(self):
		self.assertEqual(createPrettyLabel("MYSuperSpacer"), "MY Super Spacer")

	def test_9(self):
		self.assertEqual(createPrettyLabel("MySpACe"), "My Sp A Ce")
		# My Sp A Ce
	def test_A(self):
		self.assertEqual(createPrettyLabel("MySpAcE"), "My Sp Ac E")

	def test_B(self):
		self.assertEqual(createPrettyLabel(""), "")


if __name__ == '__main__':
    unittest.main()

