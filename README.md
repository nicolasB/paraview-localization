# Paraview-Localization

This repository contains drafts and tools for the localization process of Paraview.

## XML parsing utility 

`parsing.py` generates a c++ header on which lupdate can be ran in order to get a Qt translation source file.

### TODO

- [x] Parse XML files and get label or name
- [x] Parses the arguments like lupdate does
- [x] Change label as [in CreateNewPrettyLabel behavior()](https://gitlab.kitware.com/paraview/paraview/-/blob/ac89766d61c11973abba45fae791f8c267e7f785/Remoting/ServerManager/Testing/Cxx/TestSMPropertyLabel.cxx)
- [x] Find the corresponding line of the file
- [x] Add unit test to the executable
