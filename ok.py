import xml.etree.ElementTree as ET
import argparse
import os
import re

def replaceByEscapableOKs(text: str) -> str:
	res = "OK" + "".join(re.findall("%[0-9]{1,2}", text))
	if "&" in text:
		res = "&" + res
		print(text)
	return res

def recTranslations(node):

	if node.tag == "message":
		node.find('translation').attrib.pop("type", None)
		node.find('translation').text = replaceByEscapableOKs(node.find('source').text)
	else:
		for child in node:
			recTranslations(child)


def fileParsing(fileIn: str):
	xmlTree = ET.parse(fileIn)
	recTranslations(xmlTree.getroot())
	xmlTree.write(fileIn)


def forEachFile(inFiles: list):
	for i in inFiles:
		fileParsing(i)


def __main__():
	"""
	Asks for one or more input files.
	"""
	parser = argparse.ArgumentParser("Rewrites all translations in a ts by 'OK'")
	parser.add_argument('inFiles', metavar='N', nargs="+")
	args = parser.parse_args()
	for i in args.inFiles:
		if not os.path.exists(i):
			raise ValueError(f"Invalid input file '{i}'")
	forEachFile(args.inFiles)


if __name__ == "__main__":
	__main__()
